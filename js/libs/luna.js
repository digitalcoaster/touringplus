/**
 * User: Migsar Navarro
 * Date: 9/22/13
 * Time: 1:33 PM
 */

$.Luna = {
    // Activate for displaying debug messages
    debugging: true,
    settings :  {
        // Main container
        $main : $('#document-wrap'),
        // Sections stack
        sections: [],
        // An event stack, we add all elements here
        events: [],
        // Active events stack
        activeEvents: [],
        // Sets how long the scroll will be
        step: 50,
        // Current displacement position
        timeline : 0,
        // Start displacement position
        start : 0,
        // End displacement position, don't include window's height here 'cause it should be recalculated
        end: null,
        // Locks screen and transfers control to section
        scrollLock : false,
        // Design width, all elements are adjusted with absolute position for allowing animations
        designWidth: 1600
    },

    // Let's get it started
    init : function () {
        this.getSections();
        this.settings.end = this.settings.$main.height();

        this.centerElements();

        // Navigation starts on mousewheel event
        $(window).mousewheel(function(e,delta,deltaX,deltaY) {
            $.Luna.wheeline(deltaY);
            e.stopPropagation();
            return false;
        });

        if($.Scrollex) {
            $(document).bind('scrollex', this.setPosition);
        }

        $(document).keypress(function(e){
            var cancela = true;
            switch (e.key) {
                case 'Spacebar':
                    cancela = false;
                    break;
                case 'Left':
                    cancela = false;
                    break;
                case 'Up':
                    cancela = false;
                    break;
                case 'Right':
                    cancela = false;
                    break;
                case 'Down':
                    cancela = false;
                    break;
                case 'PageUp':
                    cancela = false;
                    break;
                case 'PageDown':
                    cancela = false;
                    break;
            }
            return cancela;
        });

        // todo: Change this to the event object
        // Apply default styles if needed

        $('.flip-container .flip-item').addClass('fuera');
        $('.wave li').addClass('fuera');

        if (this.debugging) {
            $(document).click(function(e){
                if (e.which == 1)
                    alert($.Luna.settings.timeline);
            });
        }
    },

    // Main function
    wheeline : function (dir) {
        // Logic is inverted because scrolling down is positive
        var scrolling = this.settings.$main.css('top'),
            position;
        if (this.debugging) console.info('Scrolling: '+-parseInt(scrolling));

        // Check which elements we'll be active
        this.setActiveEvents();
        this.settings.scrollLock = this.eventLock();

        if(!this.settings.scrollLock) {
            if(dir>0)
                this.backward(scrolling);
            else
                this.forward(scrolling);
        }

        // Calculate position and creates event
        position = this.settings.timeline/this.settings.end;

        if($.Scrollex) {
            $.Scrollex.setPosition(position);
        }

        // Runs active event's function
        var events = this.settings.activeEvents,
            stillLocked = false;
        for(var i=0; i<events.length; i++){
            // the event should return a boolean value indicating if the scroll is still locked
            // if at least one event is locking the scrollbar won't scroll
            switch (events[i].callback){
                case 'translate':
                    events[i].callback=this.translate;
                    break;
                case 'hide':
                    events[i].callback=this.hide;
                    break;
                case 'style':
                    events[i].callback=this.cssStyle;
                    break;
            }
            stillLocked = events[i].callback(dir,this.settings.timeline);
        }
        //
        this.settings.scrollLock = stillLocked;

        // Apply CSS classes for transitions
        //this.cssStyle('wave');
    },

    backward : function (position) {
        var newPosition = parseInt(position)+this.settings.step;
        // Checks if the current style.top is greater than 0
        if (newPosition>this.settings.start){
            // If it is bring it back to cero
            this.settings.timeline = -this.settings.start;
        } else {
            // Do the scrolling
            this.settings.timeline = -newPosition;
        }

        this.settings.$main.css('top',-this.settings.timeline+'px');
    },

    forward : function (position) {
        var newPosition = parseInt(position)-this.settings.step;
        // Checks if the current style.top is greater than document's height
        if (newPosition< (-this.settings.end+$(window).height())){
            // If it is set it to document's height
            this.settings.timeline = this.settings.end-$(window).height();
        } else {
            // Do the scrolling
            this.settings.timeline = -newPosition;
        }

        this.settings.$main.css('top',-this.settings.timeline+'px');
    },

    // Set the position of the content based on the percentage (0 to 1)
    setPosition: function (ev){
        var barLong = ($.Luna.settings.end - $(window).height()),
            pos = ev.originalEvent.detail;

        console.log(pos);

        if (pos < .015)
            pos = 0.0;

        if (pos > .97)
            pos = 1.0;

        $.Luna.settings.timeline = parseInt(barLong * pos);
        $.Luna.settings.$main.css('top',-$.Luna.settings.timeline+'px');
    },

    setPositionAbs: function (px){
        var barLong = ($.Luna.settings.end - $(window).height()),
            pos = px/barLong;

        if (pos < .015)
            pos = 0.0;

        if (pos > .985)
            pos = 1.0;

        $.Luna.settings.timeline = parseInt(barLong * pos);
        $.Luna.settings.$main.css('top',-$.Luna.settings.timeline+'px');
    },

    // Adds an event to the stack
    addEvent : function (LunaEvent) {
        this.settings.events.push(LunaEvent);
    },

    // Adds events to activeEvent stack
    setActiveEvents: function () {
        // Iterates over all declared events to see which one is active
        var events = this.settings.events, pos = this.settings.timeline;
        for(var i=0;i<events.length;i++){
            if ((pos >= events[i].eventStart) && (pos <= events[i].eventEnd)) {
                var activos = this.settings.activeEvents, flag=true;
                for(var j =0; j < activos.length; j++)
                    if (events[i].name == activos[j].name)
                        flag =false;
                if (flag) activos.push(events[i]);
            }
        }
        // Iterates over active events to notice if one of them shouldn't be there
        events = this.settings.activeEvents;
        for(var i=0;i<events.length;i++){
            if (pos > events[i].eventEnd || pos < events[i].eventStart) {
                events.splice(events.indexOf(events[i]),1);
            }
        }
    },

    // Returns true if at least one active event have scroll lock
    eventLock: function (){
        var events = this.settings.activeEvents, doLock=false;
        for(var i=0; i < events.length; i++){
            var current = events[i];
            if (current.lockX){
                doLock = true;
            }
        }
        return doLock;
    },

    // Translation event
    translate : function (direction, position) {
        // at this time this function is totally hand-written, hopefully will be automatized soon
        var tag = this.tag,
            property = this.property,
            timelapse=this.eventEnd - this.eventStart,
            propertylapse = this.propertyEnd - this.propertyStart,
            step = propertylapse/timelapse,
            currentPosition = position - this.eventStart,
            currentValue = this.propertyStart + parseInt(currentPosition * step);

        console.log('Trans: '+this.tag+' : '+currentPosition);
        $(tag).css(property,currentValue+'px');

        this.centerElements();
    },

    // Transparency event
    hide : function (direction, position) {
        // at this time this function is totally hand-written, hopefully will be automatized soon
        var tag = this.tag,
            property = this.property,
            timelapse=this.eventEnd - this.eventStart,
            propertylapse = this.propertyEnd - this.propertyStart,
            step = propertylapse/timelapse,
            currentPosition = position - this.eventStart,
            currentValue;

        if (propertylapse < 0) {
            step = -step;
            currentValue = 1 - (currentPosition * step);
        }else {
            currentValue = currentPosition * step;
        }
        $(tag).css(property,currentValue);
    },

    // Apply class event
    cssStyle : function (direction, position) {
        var ident = "."+this.tag+" #"+this.name;
        if (position > this.eventEnd){
            //$(ident).addClass('fuera');
        }else{
            if (position >= this.eventStart){
                $(ident).removeClass('fuera');
            }else{
                $(ident).addClass('fuera');
            }
        }
    },

    // Configure sections with data-Luna info
    getSections : function () {
        this.settings.sections = $('.section-luna').each(function(i, section){
            var attrs = eval('('+$(section).attr('data-Luna')+')');
//                console.info(attrs);
            $(section).css({
                'width': $(window).width(),
                'height': attrs.height,
                'position': 'relative',
                'background': 'transparent url("'+attrs.background+'") top center no-repeat'
            });
        });
    },

    // Center absolute positioned elements
    centerElements : function () {
        var screenWidth = $(window).width(),
            designWidth = this.settings.designWidth,
            offscreen = parseInt((designWidth - screenWidth)/ 2);

        $('.luna-item-left').each(function(index,items){
            var $item = $(this), l = parseInt($item.css('left'));
            $item.css('left',(l-offscreen)+'px');
        });

        $('.luna-item-right').each(function(index,items){
            var $item = $(this), r = parseInt($item.css('right'));
            $item.css('right',(r-offscreen)+'px');
        });
        /*$('.luna-item').each(function(index,items){
            var $item = $(this), r = parseInt($item.css('right')), l = parseInt($item.css('left'));

            console.log($(this).css('left'));
            console.log($(this).css('right'));
            if (l > r) {
                console.log('Nunca entré aquí');
                $item.css('right',(r-offscreen)+'px');
            }else {
                console.log('Ri');
                $item.css('left',(l-offscreen)+'px');
            }
        });*/
    }
};

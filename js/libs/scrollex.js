/**
 * User: Migsar Navarro
 * Date: 9/22/13
 * Time: 1:34 PM
 */

$.Scrollex = {
    $scrollbar : $('#scrollex-scrollbar'),
    $scroll : $('#scrollex-scroll'),
    init : function (tag) {
        if (tag){
            this.$scrollbar = $(tag+'-scrollbar');
            this.$scroll = $(tag+'-scroll');
        }
        var scrollbar = this.$scrollbar,
            scroll = this.$scroll,
            scrollPressed,
            event;


        scrollbar.css('height',$(window).height()+'px');

        scroll.mousedown(function(e){
            if (e.which==1){
                scrollPressed = true;
                e.stopPropagation();
            }
            return false;
        });

        $(window).mouseup(function(){
            scrollPressed = false;
        });

        $(window).mousemove(function(e){
            var y = e.pageY,
                sbTop = scrollbar.offset().top,
                sbBottom = sbTop + scrollbar.height();

            if (scrollPressed){
                if((y >= sbTop) && (y <= (sbBottom - scroll.height()))){
                    var pos = (y - sbTop)/(sbBottom - sbTop);
                    scroll.css('top', y+'px');
                    event = new CustomEvent('scrollex',{'detail': pos});
                    document.dispatchEvent(event);
                }
            }
        });
    },
    // Set position given as a floating point between 0 and 1
    setPosition: function (pos) {
        console.log('S'+pos);
        var scroll = this.$scroll,
            barLong = this.$scrollbar.height() - scroll.height();

        scroll.css('top', (parseInt(pos*barLong))+'px');
    }
};

jQuery.fn.reverse = [].reverse;

;( function( $, window, undefined ) {

    'use strict';

    $.Luna.init();
    $.Scrollex.init('#global');


    // Translation event
    $.Luna.addEvent({
        'name': 'Main Menu',
        'tag': '#global-menu',
        'property': 'left',
        'eventStart':0,
        'eventEnd': 958,
        'propertyStart':0,
        'propertyEnd': -105,
        'callback': 'translate',
        'lockX': false
    });

    $.Luna.addEvent({
        'name': 'Texto 2',
        'tag': '#section-2 .texto-1 p',
        'property': 'top',
        'eventStart':300,
        'eventEnd': 1035,
        'propertyStart':-100,
        'propertyEnd': 170,
        'callback': 'translate',
        'lockX': false
    });

    $.Luna.addEvent({
        'name': 'Venecia 2',
        'tag': '#section-4 #venecia-2 img',
        'property': 'top',
        'eventStart':2530,
        'eventEnd': 3120,
        'propertyStart':-730,
        'propertyEnd': 0,
        'callback': 'translate',
        'lockX': false
    });

    $.Luna.addEvent({
        'name': 'Texto imagina',
        'tag': '#section-5 .texto-1 p',
        'property': 'top',
        'eventStart':3030,
        'eventEnd': 3810,
        'propertyStart':-145,
        'propertyEnd': 60,
        'callback': 'translate',
        'lockX': false
    });
//    $.Luna.addEvent({
//        'name': 'Imagina 1',
//        'tag': '#section-5 #idea-1',
//        'property': 'top',
//        'eventStart':2530,
//        'eventEnd': 3120,
//        'propertyStart':-637,
//        'propertyEnd': 0,
//        'callback': 'translate',
//        'lockX': false
//    });
//    $.Luna.addEvent({
//        'name': 'Imagina 2',
//        'tag': '#section-5 #idea-2',
//        'property': 'top',
//        'eventStart':2530,
//        'eventEnd': 3120,
//        'propertyStart':-637,
//        'propertyEnd': 0,
//        'callback': 'translate',
//        'lockX': false
//    });
//    $.Luna.addEvent({
//        'name': 'Imagina 3',
//        'tag': '#section-5 #idea-3',
//        'property': 'top',
//        'eventStart':2530,
//        'eventEnd': 3120,
//        'propertyStart':-637,
//        'propertyEnd': 0,
//        'callback': 'translate',
//        'lockX': false
//    });
//    $.Luna.addEvent({
//        'name': 'Imagina 4',
//        'tag': '#section-5 #idea-4',
//        'property': 'top',
//        'eventStart':2530,
//        'eventEnd': 3120,
//        'propertyStart':-637,
//        'propertyEnd': 0,
//        'callback': 'translate',
//        'lockX': false
//    });
//    // Transparency event
//    $.Luna.addEvent({
//        'name': 'Main Menu',
//        'tag': '#global-menu',
//        'property': 'opacity',
//        'eventStart':0,
//        'eventEnd': 958,
//        'propertyStart':1.0,
//        'propertyEnd': 0.0,
//        'callback': 'hide',
//        'lockX': false
//    });

    // Flip events

    $.Luna.addEvent({'name': 'ft-uno','tag': 'flip-container','eventStart':250,'eventEnd': 750,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'ft-dos','tag': 'flip-container','eventStart':1350,'eventEnd': 1850,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'ft-tres','tag': 'flip-container','eventStart':2250,'eventEnd': 2750,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'ft-cuatro','tag': 'flip-container','eventStart':3020,'eventEnd': 3520,'callback': 'style','lockX': false});

    // Destinos' events

    $.Luna.addEvent({'name': 'china','tag': 'wave','eventStart':8944,'eventEnd': 9744,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'polo-norte','tag': 'wave','eventStart':9182,'eventEnd': 9982,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'egipto','tag': 'wave','eventStart':9420,'eventEnd': 10220,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'emiratos-arabes','tag': 'wave','eventStart':9658,'eventEnd': 10458,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'europa','tag': 'wave','eventStart':9896,'eventEnd': 10796,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'india','tag': 'wave','eventStart':10134,'eventEnd': 10934,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'indochina','tag': 'wave','eventStart':10372,'eventEnd': 11172,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'jordania','tag': 'wave','eventStart':10610,'eventEnd': 11410,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'marruecos','tag': 'wave','eventStart':10848,'eventEnd': 11648,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'polinesia','tag': 'wave','eventStart':11086,'eventEnd': 11886,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'himalaya','tag': 'wave','eventStart':11324,'eventEnd': 12124,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'rusia','tag': 'wave','eventStart':11562,'eventEnd': 12362,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'africa','tag': 'wave','eventStart':11800,'eventEnd': 12600,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'turquia','tag': 'wave','eventStart':12038,'eventEnd': 12838,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'trenes-legendarios','tag': 'wave','eventStart':12276,'eventEnd': 13076,'callback': 'style','lockX': false});
    $.Luna.addEvent({'name': 'cruceros-fluviales','tag': 'wave','eventStart':12514,'eventEnd': 13314,'callback': 'style','lockX': false});

    // Event's callback functions

    function testFn (direction, position) {
        // Mousewheel moving up
        if(direction>0) {
        }
        // Mousewheel moving down
        else {
        }
        alert('Funcion 2');
    }

    var clock = $('#count-uno').FlipClock({
    });

})(jQuery, window);
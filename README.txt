This is a project to use the scroll as navigation, every mousewheel event is intercepted and propagation is stopped.

Scrollbar is disabled on the CSS file so we have to take care of everything.

We have to stacks, one for the sections and one for the events.

When we register a section we add it to the sections stack, the parameters are:

-id

-style

It is possible to use the push method or to assign an array of sections.

When we register an event we add it to the events stack, the parameters are: